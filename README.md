# Mock Shopping Cart (PHP)

##Description
A mock shopping card specific for this technical challenge. Functions included:
 - Simple frontend with form validation in 3 pages, index.php, makePayment.php, and checkPayment.php
 - Bootstrap and jQuery applied to the frontend pages
 - Designed to display spinning icon when waiting for server response after form submitted
 - Show Lightbox to user for server respond 
 - New payment gateway can be add easily by adding new Gateway class to lib/MockCart/Payment
 - Valid payment record will be stored in both Redis and MySQL databases
 - Payment record checking always perform at Redis first, if record not found fall back to MySQL and make a copy back to Redis
 - Files under **api** directory are the API endpoint for both payment making and checking function
 - Making Payment API endpoint determines which Gateway to use based on the requirement of this challenge
 
 ## Deployment
  - Deploy by Docker automatically at build time.
  - Start page: [http://localhost/mockcat/](http://localhost/mockcat/)

## Redis Data Structure
 - Data stored as Redis HASH
 - Key format: **CUSTOMER_NAME:PAYMENT_REFERENCE**

## MySQL Data Structure
```mysql
CREATE TABLE `payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(64) NOT NULL DEFAULT '',
  `customer_phone` varchar(64) NOT NULL DEFAULT '',
  `currency` int(3) NOT NULL,
  `price` decimal(20,2) NOT NULL DEFAULT 0.00,
  `reference` varchar(20) NOT NULL DEFAULT 'Rejected',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payment_unique` (`customer_name`,`reference`,`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```


 
## Third-party libraries
 - [PHP Credit Card Validator](https://github.com/inacho/php-credit-card-validator)