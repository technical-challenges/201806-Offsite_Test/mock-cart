<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <style>
        .btn {
            margin: 5px;
        }

        .container {
            max-width: 960px;
        }
    </style>
    <title>MockCart</title>
</head>
<body>
<div class="container">
    <div class="py-3 text-center">
        <h2>MockCart</h2>
    </div>
    <div class="row text-center">
        <div class="col-md-10 offset-md-1 order-md-1">
            <a class="btn btn-primary btn-lg btn-block" href="makePayment.php" role="button">Make Payment</a>
            <a class="btn btn-success btn-lg btn-block" href="checkPayment.php" role="button">Check Payment</a>
        </div>
    </div>
    <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2018 blademirage</p>
    </footer>
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>