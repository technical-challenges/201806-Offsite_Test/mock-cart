<?php

namespace Unicon\Utility;

use Redis;
use Exception;

class Cache
{
    private $_config;
    private $_redisClient;

    public function __construct($config)
    {
        $this->_redisClient = new Redis();
        $this->_config = $config;
        $this->openRedisConnection();
    }

    function openRedisConnection()
    {
        $this->_redisClient->connect($this->_config['host'], $this->_config['port']);
    }

    function setValue($key, $value, $ttl)
    {

        try {
            $this->_redisClient->setex($key, $ttl, $value);
        } catch (Exception $e) {
            error_log("[Exception] " . $e->getMessage());
        }
    }

    function getValue($key)
    {
        try {
            return $this->_redisClient->get($key);
        } catch (Exception $e) {
            error_log("[Exception] " . $e->getMessage());
        }
    }

    function setHash($key, array $hash)
    {
        try {
            $this->_redisClient->hMset($key, $hash);
        } catch (Exception $e) {
            error_log("[Exception] " . $e->getMessage());
        }
    }

    function getHash($key)
    {
        try {
            return $this->_redisClient->hGetAll($key);
        } catch (Exception $e) {
            error_log("[Exception] " . $e->getMessage());
        }
    }

    function isKeyExists($key)
    {
        try {
            return $this->_redisClient->exists($key);
        } catch (Exception $e) {
            error_log("[Exception] " . $e->getMessage());
        }

    }
}