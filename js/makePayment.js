"use strict";
const API_ENDPOINT = urlChunks + "/api/makePayment.php";
const FORM_ID = "#payment-form";

$(document).ready(function () {
    $('#payment-form').parsley({});

    $("#submit-payment").click(function (e) {
        e.preventDefault();
        if ($(FORM_ID).parsley().validate()) {
            // console.log("Valid form");
            setOverlay(true);
            insertItem();
        } else {
            // console.log("Invalid form");
        }
    });

});

function insertItem() {
    let formData = new FormData($(FORM_ID)[0]);
    let formObj = {};
    formData.forEach(function (value, key) {
        formObj[key] = value;
    });
    let jsonData = JSON.stringify(formObj);
    sendData(API_ENDPOINT, jsonData, insertItemSuccess, insertItemFail, true);
}

var insertItemSuccess = function (res) {
    // console.log(res);
    setOverlay(false);
    setModal("Payment Accepted", "Reference code: " + res.reference);
    $(FORM_ID).parsley().reset();
    $(FORM_ID)[0].reset();
}

var insertItemFail = function (res) {
    // console.log(res);
    setOverlay(false);
    setModal("Payment Rejected", "Reason: " + res.message);
    $(FORM_ID).parsley().reset();
    $(FORM_ID)[0].reset();
}