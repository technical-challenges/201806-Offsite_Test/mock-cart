<?php

namespace Unicon\Payment;

abstract class Gateway
{
    const ENDPOINT = "";
    protected $_carnNumber;
    protected $_code;
    protected $_year;
    protected $_month;
    protected $_price;
    protected $_currency;

    /**
     * Gateway constructor.
     * @param $paymentData
     */
    public function __construct(array $paymentData)
    {
        $this->_carnNumber = $paymentData['card_number'];
        $this->_code = $paymentData['code'];
        $this->_year = $paymentData['year'];
        $this->_month = $paymentData['month'];
        $this->_price = $paymentData['price'];
        $this->_currency = $paymentData['currency'];
    }

    abstract public function MakePayment();

    /**
     * @return mixed
     */
    public function getCarnNumber()
    {
        return $this->_carnNumber;
    }

    /**
     * @param mixed $carnNumber
     */
    public function setCarnNumber($carnNumber)
    {
        $this->_carnNumber = $carnNumber;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->_year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->_year = $year;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->_month;
    }

    /**
     * @param mixed $month
     */
    public function setMonth($month)
    {
        $this->_month = $month;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->_price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->_price = $price;
    }
}