<?php

namespace MockCart\Payment;

use Exception;
use PDO;
use Unicon\Utility\Database;

class Payment
{
    const TABLE = "payment";
    const LIST_ITEM_FIELD = "id,customer_name,customer_phone,currency,price,reference,datetime";
    const INSERT_ITEM_FIELD = "customer_name,customer_phone,currency,price,datetime";
    const INSERT_ITEM_OPTIONAL_FIELD = "reference";
    const LIST_ITEM_KEY = "id,customer_name,customer_phone,currency,price,reference,datetime";

    private $_client;

    public function __construct(Database $client)
    {
        $this->_client = $client;
    }

    public function listAllItem($orderBy = 0)
    {
        $order = "id ASC";
        if ($orderBy !== 0) {
            $order = "datetime DESC";
        }
        $query = "SELECT " . self::LIST_ITEM_FIELD . " FROM " . self::TABLE . " ORDER BY $order";
        $res = $this->_client->queryData($query);
        if ($res === false) {
            throw new Exception("Failed to query " . self::TABLE . " data.");
        }
        return $res;
    }

    public function listItem($data, $orderBy = 0)
    {
        try {
            $order = "id ASC";
            if ($orderBy !== 0) {
                $order = "datetime DESC";
            }
            $validKeys = array();
            if ($this->_checkOptional($data, self::LIST_ITEM_KEY, $validKeys)) {
                $values = array();
                $input = $this->_formatInput($data, self::LIST_ITEM_KEY, true);
                $this->_client->addFieldValueMapping($input, $fieldNames, $values);
                $conditions = $this->_formatCondition($input, $validKeys);
                $conditionStr = "";
                if (!empty($conditions)) {
                    $conditionStr = " AND " . implode(" AND ", $conditions);
                }
                $query = "SELECT " . self::LIST_ITEM_FIELD . " FROM " . self::TABLE . " WHERE 1$conditionStr ORDER BY $order";
                $res = $this->_client->queryData($query, $values);
                if ($res === false) {
                    throw new Exception("Failed to query " . self::TABLE . " data.");
                }
                return $res;
            } else {
                throw new Exception("Failed to query " . self::TABLE . " data with keys.");
            }

        } catch (Exception $e) {
            throw new Exception("Failed to query " . self::TABLE . " data with keys.");
        }
    }

    public function insertItem($data)
    {
        try {
            if ($this->_checkMandatory($data)) {
                $fieldNames = array();
                $values = array();
                $input = $this->_formatInput($data, self::INSERT_ITEM_FIELD . "," . self::INSERT_ITEM_OPTIONAL_FIELD, true);
                $this->_client->addFieldValueMapping($input, $fieldNames, $values);
                $columns = implode(",", $fieldNames);
                $holders = implode(",", array_keys($values));
                $query = "INSERT INTO " . self::TABLE . "($columns) VALUES ($holders)";
                $queryArray = array(Database::KEY_QUERY => $query, Database::KEY_VALUES => $values);
                $id = $this->_client->insertData($queryArray, true);
                if ($id === 0 || $id === false) {
                    throw new Exception("Failed to insert new " . self::TABLE . " record.");
                }
                return $id;
            } else {
                throw new Exception("Invalid/Empty item specified.");
            }
        } catch (Exception $e) {
            throw new Exception("Failed to insert " . self::TABLE . " data.");
        }
    }

    private function _checkMandatory($data)
    {
        $columns = explode(",", self::INSERT_ITEM_FIELD);
        $res = true;
        foreach ($columns as $col) {
            $res &= (isset($data[$col]) && !empty($data[$col]));
        }
        return $res;
    }

    private function _checkOptional($data, $keys, &$validKeys)
    {
        $columns = explode(",", $keys);
        $res = true;
        $hasValidKey = false;
        foreach ($columns as $col) {
            if (isset($data[$col])) {
                $res = ($res && (!empty($data[$col])));
                $hasValidKey = ($hasValidKey || $res);
                if (!empty($data[$col])) {
                    $validKeys[] = $col;
                }
            }
        }
        return ($res && $hasValidKey);
    }

    private function _formatInput($data, $columnKeys = self::INSERT_ITEM_FIELD, $optionalKeys = false)
    {
        $input = array();
        $columns = explode(",", $columnKeys);
        foreach ($columns as $col) {
            if ($optionalKeys === true && !isset($data[$col])) {
                continue;
            }
            $type = PDO::PARAM_STR;
            if (is_numeric($data[$col])) {
                $type = PDO::PARAM_INT;
            }
            $input[] = $this->_client->formatInput($col, $type, $data[$col]);
        }
        return $input;
    }

    private function _formatCondition($input, $validKeys)
    {
        $conditions = [];
        foreach ($validKeys as $v) {
            $key = array_search($v, array_column($input, 'name'));
            $holder = ":" . $input[$key]["name"];
            $conditions[] = $input[$key]["name"] . " = " . $holder;

        }
        return $conditions;
    }
}