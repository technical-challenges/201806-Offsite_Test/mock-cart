<?php
require_once('common/common.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/parsleyjs/src/parsley.css">
    <link rel="stylesheet" href="vendors/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/common.css">
    <title>MockCart</title>
</head>
<body>
<div class="container">
    <div class="py-3 text-center">
        <h2>Check Payment</h2>
    </div>
    <form id="payment-form" class="needs-validation" novalidate>
        <div class="row text-left">
            <div class="col-md-10 offset-md-1 order-md-1">
                <h4 class="mb-3">Payment Details</h4>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="customer_name">Customer Name</label>
                        <input class="form-control" type="text" name="customer_name" id="customer_name" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="reference">Payment reference code</label>
                        <input class="form-control" type="text" name="reference" id="reference" required>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="button" id="submit-payment">Check Payment
                </button>
                <hr class="mb-4">
            </div>
        </div>
    </form>


</div>
<footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; <?php echo($now->format('Y')); ?> blademirage</p>
</footer>
<?php include('common/lightbox.php'); ?>

</body>
<script src="vendors/jquery/dist/jquery.min.js"></script>
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendors/parsleyjs/dist/parsley.min.js"></script>
<script src="js/common.js"></script>
<script src="js/checkPayment.js"></script>
</html>