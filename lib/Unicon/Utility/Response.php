<?php

namespace Unicon\Utility;


class Response
{
    const MIME_HTML = "text/html";
    const MIME_JSON = "application/json";
    const MIME_PDF = "application/pdf";

    public static function RespondError($message, $code = 0, $saveLog = true)
    {
        if ($saveLog === true) {
            error_log($message);
        }
        $error = array();
        $error["code"] = $code;
        $error["message"] = $message;
        header("Content-Type: " . self::MIME_JSON . "; charset=UTF-8");
        self::_Respond(json_encode(array("error" => $error)), 400);
    }

    public static function RespondData($data)
    {
        header("Content-Type: " . self::MIME_JSON . "; charset=UTF-8");
        self::_Respond(json_encode(array("data" => $data)), 200);
    }

    public static function RespondRawData($data)
    {
        header("Content-Type: " . self::MIME_JSON . "; charset=UTF-8");
        self::_Respond(json_encode($data), 200);
    }

    public static function RespondHtml($html)
    {
        header("Content-Type: " . self::MIME_HTML . "; charset=utf-8");
        self::_Respond($html, 200, true);
    }

    public static function RespondFileStream($fileData, $fileName)
    {
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        header('Content-Type: ' . "application/octet-stream");
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        // header('Content-Length: ' . $length);
        self::_Respond($fileData, 200, true);
    }

    private static function _Respond($data, $httpStatus, $noCache = false)
    {
        if (function_exists("http_response_code")) {
            http_response_code($httpStatus);
        } else {
            header("HTTP/1.1 $httpStatus", true, $httpStatus);
        }

        if ($noCache === true) {
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
            header("Cache-Control: no-cache");
            header("Pragma: no-cache");
        }
        echo($data);
    }
}