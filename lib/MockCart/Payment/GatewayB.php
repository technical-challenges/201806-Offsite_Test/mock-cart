<?php

namespace MockCart\Payment;

use Unicon\Payment\Gateway;
use Unicon\Utility\Request;

class GatewayB extends Gateway
{
    const ENDPOINT = "http://php-apache/GATEWAY_B/makePayment.php";

    public function __construct($paymentData)
    {
        parent::__construct($paymentData);
        // error_log(__CLASS__ . "Processing");
    }


    public function MakePayment()
    {
        $data = array(
            'number' => $this->_carnNumber,
            'code' => $this->_code,
            'year' => $this->_year,
            'month' => $this->_month,
            'price' => $this->_price
        );
        try {
            $result = Request::CallPost(static::ENDPOINT, $data);
            $result = json_decode($result, true);
            $record = array('success' => false, 'reference' => "");
            if (isset($result["data"])) {
                $record['success'] = true;
                $record['reference'] = $result["data"]['reference'];
            } else {
                $record['reference'] = $result["error"]['message'];
            }
            return $record;
        } catch (Exception $e) {
            error_log("[" . __CLASS__ . " Exception] " . $e->getMessage());
            return false;
        }
    }
}