<?php

namespace Unicon\Utility;

use PDO;
use Exception;

class Database
{
    const KEY_DATABASE = "database";
    const KEY_HOST = "host";
    const KEY_QUERY = "query";
    const KEY_VALUES = "values";
    const KEY_VALUE = "value";
    const KEY_TYPE = "type";
    const PLACE_HOLDER_PREFIX = ":";


    private $_config;
    private $_mySQLClient;

    public function __construct($config, $username = null, $password = null)
    {
        $this->createMySQLClient($config, $username, $password);
        if ($this->_mySQLClient === false) {
            throw new Exception("Failed to initialize mysql client.");
        }
    }

    protected function createMySQLClient($config, $username = null, $password = null)
    {
        if (is_array($config)) {
            $dbConfigJSONObj = $config;
        } else {
            if (!file_exists($config)) {
                error_log("Please set up $config file.\n");
                return false;
            }
            $dbConfigJSONObj = json_decode(file_get_contents($config), true);
        }
        $this->_config = $dbConfigJSONObj;

        $dbDSN = "mysql:dbname=" . $dbConfigJSONObj[self::KEY_DATABASE] . ";host=" . $dbConfigJSONObj[self::KEY_HOST] . ";charset=utf8";
        if (is_null($username)) {
            $dbUsername = $dbConfigJSONObj[self::KEY_USERNAME];
        } else {
            $dbUsername = getEnv($username);
        }
        if (is_null($password)) {
            $dbPassword = $dbConfigJSONObj[self::KEY_PASSWORD];
        } else {
            $dbPassword = getEnv($password);
        }

        try {
            $this->_mySQLClient = new PDO($dbDSN, $dbUsername, $dbPassword);
            $this->_mySQLClient->exec("set names utf8");
        } catch (PDOException $e) {
            error_log("Failed to connect to MySQL: " . $e->getMessage() . "\n");
            return false;
        }
        return true;
    }

    protected function prepareQuery($query, $values = array())
    {
        $stmt = $this->_mySQLClient->prepare($query);
        if ($stmt === false) {
            $errorArray = $this->_mySQLClient->errorInfo();
            error_log("An error has occurred when preparing query: " . $errorArray[2] . ".");
            return false;
        }
        foreach ($values as $k => $v) {
            $value = $v[self::KEY_VALUE];
            $type = $v[self::KEY_TYPE];
            $typeStr = "";
            if ($type === PDO::PARAM_INT) {
                $typeStr = "INT";
            } elseif ($type === PDO::PARAM_STR) {
                $typeStr = "STR";
            }
            $stmt->bindValue($k, $value, $type);
            if ($stmt === false) {
                error_log("Failed to bind value to query: [$k:$value:$typeStr]");
                return false;
            }
        }
        return $stmt;
    }

    protected function executeQuery($stmt)
    {
        $result = null;
        if ($stmt->execute()) {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $errorArray = $stmt->errorInfo();
            error_log("An error has occurred when executing query: " . $errorArray[2] . ".");
            return false;
        }
        return $result;
    }

    public function queryData($query, $mapping = array())
    {
        $stmt = $this->prepareQuery($query, $mapping);
        if ($stmt === false) {
            return false;
        }
        $result = $this->executeQuery($stmt);
        if ($result === false) {
            return false;
        }
        return $result;
    }

    public function insertData(array $queryArray, $getLastInsertId = false)
    {
        $query = $queryArray[self::KEY_QUERY];
        $mapping = $queryArray[self::KEY_VALUES];
        $stmt = $this->prepareQuery($query, $mapping);
        if ($stmt === false) {
            return false;
        }
        $result = $stmt->execute();
        if ($result === false) {
            $errorArray = $stmt->errorInfo();
            if ($errorArray[1] === 1062) {
                throw new Exception("Duplicated key.");
            } else {
                throw new Exception("An error has occurred when execute query \"" . $stmt->queryString . "\" [" . $errorArray[2] . "].");
            }

        }
        if ($getLastInsertId === true) {
            $result = $this->_mySQLClient->lastInsertId();
        }
        return $result;
    }

    public function doTransaction($queries, $returnLastQueryResult = false)
    {
        $stmts = array();
        foreach ($queries as $query) {
            $values = array();
            if (array_key_exists(self::KEY_VALUES, $query)) {
                $values = $query[self::KEY_VALUES];
            }
            $stmts[] = $this->prepareQuery($query[self::KEY_QUERY], $values);
        }
        try {
            $i = 0;
            $this->_mySQLClient->beginTransaction();
            $res = false;
            foreach ($stmts as $stmt) {
                $res = $stmt->execute();
                if ($res === false) {
                    $errorArray = $stmt->errorInfo();
                    // var_dump($errorArray);
                    $this->_mySQLClient->rollBack();
                    if ($errorArray[1] === 1062) {
                        // error_log(__LINE__.PHP_EOL);
                        throw new Exception("Duplicated key.", Exception::DATABASE_DUPLICATED_KEY);
                    } else {
                        throw new Exception("An error has occurred when execute query \"" . $stmt->queryString . "\" [" . $errorArray[2] . "].");
                    }
                }
                $i++;
            }
            $this->_mySQLClient->commit();
            if ($returnLastQueryResult === true) {
                $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
                return $res;
            } else {
                return true;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
        return false;
    }

    public function getConfig()
    {
        return $this->_config;
    }

    public function quote($input)
    {
        return $this->_mySQLClient->quote($input);
    }


    public function addFieldValueMapping(array $input, &$fieldNames, &$values)
    {
        foreach ($input as $v) {
            $holder = ":" . $v["name"];
            $fieldNames[] = $v["name"];
            $values[$holder] = array(self::KEY_TYPE => $v["type"], self::KEY_VALUE => $v["value"]);
        }
    }

    public function formatInput($name, $type, $value)
    {
        return array("name" => $name, "type" => $type, "value" => $value);
    }
}