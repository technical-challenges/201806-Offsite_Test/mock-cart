"use strict";
const JSON_TYPE = "application/json; charset=UTF-8";

var urlChunks = location.href.split('/');
console.log(urlChunks);
urlChunks = urlChunks.slice(3, urlChunks.length - 1);
urlChunks = urlChunks.join("/");
if (urlChunks != "") {
    urlChunks = "/" + urlChunks;
}

function sendData(url, data, successCallback, failCallback, isJson, extraParam) {
    let ajaxObj = {
        type: "POST",
        url: url,
        processData: false,
        cache: false,
        data: data,
        success: function (response, status, jqXHR) {
            let res = response.data;
            console.log("Success", res);
            if (successCallback) {
                console.log(extraParam);
                successCallback(res, extraParam);
            }
        },
        error: function (jqXHR, status, errorThrown) {
            let res = jqXHR.responseJSON.error;
            console.log("Failed", res);
            if (failCallback) {
                failCallback(res, extraParam);
            }
        }
    };
    if (isJson === true) {
        ajaxObj.contentType = JSON_TYPE;
        ajaxObj.dataType = "json";
    } else {
        ajaxObj.contentType = false;
    }
    //console.log(ajaxObj);
    $.ajax(ajaxObj);
}

function setOverlay(enable) {
    if (enable === true) {
        $(".overlay-white").show();
        $(".process-message").show();
    } else {
        $(".overlay-white").hide();
        $(".process-message").hide();
    }
}

function setModal(title, message) {
    $("#result-modal-title").text(title);
    $("#result-modal-body").html(message);
    $('#result-modal').modal('show');
}