<?php
session_start();
require_once("../vendor/autoload.php");
require_once("../common/common.php");
require_once("../common/db.php");


use Unicon\Utility\Cache;
use Unicon\Utility\Response;
use MockCart\Payment\Payment;
use MockCart\Payment\GatewayA;
use MockCart\Payment\GatewayB;
use Inacho\CreditCard;

const REQUIRED_FIELDS = array('card_holder_name', 'card_number', 'code', 'currency', 'customer_name', 'customer_phone', 'month', 'price', 'year');
const CURRENCY = array(
    1 => 'HKD',
    2 => 'USD',
    3 => 'AUD',
    4 => 'EUR',
    5 => 'JPY',
    6 => 'CNY'
);
try {
    $payload = json_decode(file_get_contents('php://input'), true);

    $paymentData = array();
    if (CheckPayload(REQUIRED_FIELDS, $payload, $paymentData) === false) {
        throw new Exception("Invalid parameters.");
    }

    $card = CreditCard::validCreditCard($paymentData['card_number']);
    if ($card['type'] === 'amex' || intval($paymentData['currency']) !== 1) {
        // error_log("Using Gateway_A");
        $paymentGateway = new GatewayA($paymentData);
    } else {
        // error_log("Using Gateway_B");
        $paymentGateway = new GatewayB($paymentData);
    }
    $payment = new Payment($db);
    $paymentData["datetime"] = $now->format('Y-m-d H:i:s');
    $paymentResult = $paymentGateway->MakePayment();
    if ($paymentResult['success'] === true) {
        $paymentData["reference"] = $paymentResult['reference'];
    }
    $id = $payment->insertItem($paymentData);
    if ($paymentResult['success'] === false) {
        throw new Exception($paymentResult['reference'], 400);
    } else {
        try {
            $redis = new Cache(array('host' => getenv('REDIS_HOST'), 'port' => getenv('REDIS_PORT')));
            $key = $paymentData['card_holder_name'] . ":" . $paymentResult['reference'];
            $hash = array(
                'phone' => $paymentData['customer_phone'],
                'currency' => CURRENCY[$paymentData['currency']],
                'price' => $paymentData['price']
            );
            $redis->setHash($key, $hash);
        } catch (Exception $e) {
            error_log($e->getMessage());
        }

        Response::RespondData($paymentResult);
    }


} catch (Exception $e) {
    Response::RespondError($e->getMessage(), $e->getCode(), true);
}
exit();

########################################
function CheckPayload($required, $payload, array &$data)
{
    $res = true;
    foreach ($required as $f) {
        $res = $res && (isset($payload[$f]) && !empty($payload[$f]));
        $data[$f] = $payload[$f];
    }
    return $res;
}