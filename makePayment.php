<?php
require_once('common/common.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/parsleyjs/src/parsley.css">
    <link rel="stylesheet" href="vendors/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/common.css">
    <title>MockCart</title>
</head>
<body>
<div class="container">
    <div class="py-3 text-center">
        <h2>Make Payment</h2>
    </div>
    <form id="payment-form" class="needs-validation" novalidate>
        <div class="row text-left">
            <div class="col-md-10 offset-md-1 order-md-1">
                <h4 class="mb-3">Order</h4>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="customer_name">Customer Name</label>
                        <input class="form-control" type="text" name="customer_name" id="customer_name" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="customer_phone">Customer Phone</label>
                        <input class="form-control" type="number" name="customer_phone" id="customer_phone" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="currency">Currency</label>
                        <select class="custom-select d-block w-100" name="currency" id="currency" required>
                            <option value="0">Select a currency</option>
                            <option value="1">HKD</option>
                            <option value="2">USD</option>
                            <option value="3">AUD</option>
                            <option value="4">EUR</option>
                            <option value="5">JPY</option>
                            <option value="6">CNY</option>
                        </select>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="price">Price</label>
                        <input class="form-control" type="number" name="price" id="price" min="0.00" max="999999999.99"
                               required>
                    </div>
                </div>
                <h4 class="mb-3">Payment</h4>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="card_holder_name">Card Holder Name</label>
                        <input class="form-control" type="text" name="card_holder_name" id="card_holder_name" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="card_number">Card Number</label>
                        <input class="form-control" type="number" name="card_number" id="card_number" required>
                    </div>
                    <div class="col-md-12 mb-3">
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="month">Expiration Month</label>
                                <input class="form-control" type="number" name="month" id="month" min="1" max="12"
                                       required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="year">Expiration Year</label>
                                <input class="form-control" type="number" name="year" id="year" min="2018" max="2048"
                                       required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="code">CVV</label>
                                <input class="form-control" type="number" name="code" id="code" min="0" max="9999"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="button" id="submit-payment">Make Payment
                </button>
                <hr class="mb-4">
            </div>
        </div>
    </form>
</div>
<footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">&copy; <?php echo($now->format('Y')); ?> blademirage</p>
</footer>
<?php include('common/lightbox.php'); ?>

</body>
<script src="vendors/jquery/dist/jquery.min.js"></script>
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="vendors/parsleyjs/dist/parsley.min.js"></script>
<script src="js/common.js"></script>
<script src="js/makePayment.js"></script>
</html>