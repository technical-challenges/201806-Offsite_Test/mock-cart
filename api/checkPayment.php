<?php
session_start();
require_once("../vendor/autoload.php");
require_once("../common/common.php");
require_once("../common/db.php");

use Unicon\Utility\Cache;
use Unicon\Utility\Response;
use MockCart\Payment\Payment;

const REQUIRED_FIELDS = array('customer_name', 'reference');
const CURRENCY = array(
    1 => 'HKD',
    2 => 'USD',
    3 => 'AUD',
    4 => 'EUR',
    5 => 'JPY',
    6 => 'CNY'
);
try {
    $payload = json_decode(file_get_contents('php://input'), true);
    $paymentData = array();
    if (CheckPayload(REQUIRED_FIELDS, $payload, $paymentData) === false) {
        throw new Exception("Invalid parameters.");
    }

    $redis = new Cache(array('host' => getenv('REDIS_HOST'), 'port' => getenv('REDIS_PORT')));
    $key = $paymentData['customer_name'] . ":" . $paymentData['reference'];

    if ($redis->isKeyExists($key)) {
        $res = $redis->getHash($key);
    } else {
        $payment = new Payment($db);
        $res = $payment->listItem($paymentData);
        if (!empty($res)) {
            $res = $res[0];
            $hash = array(
                'phone' => $res['customer_phone'],
                'currency' => CURRENCY[$res['currency']],
                'price' => $res['price']
            );
            $redis->setHash($key, $hash);
        } else {
            throw new Exception("Payment record not found in DB.", 400);
        }
    }
    $order = array_merge($paymentData, $res);
    Response::RespondData($order);
} catch (Exception $e) {
    Response::RespondError($e->getMessage(), $e->getCode(), true);
}
exit();

########################################
function CheckPayload($required, $payload, array &$data)
{
    $res = true;
    foreach ($required as $f) {
        $res = $res && (isset($payload[$f]) && !empty($payload[$f]));
        $data[$f] = $payload[$f];
    }
    return $res;
}