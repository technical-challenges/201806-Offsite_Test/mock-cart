<?php
date_default_timezone_set("Asia/Hong_Kong");
if (!defined("PROJECT_ROOT")) {
    $commonFileName = str_replace(dirname(__DIR__) . "/", "", __FILE__);
    $parentPaths = explode("/", str_replace("/common.php", "", $_SERVER["PHP_SELF"]));
    $subRoot = "";
    $docRoot = $_SERVER["DOCUMENT_ROOT"];
    if (!file_exists($docRoot . "/$subRoot$commonFileName")) {
        foreach ($parentPaths as $p) {
            $subRoot .= "$p/";
            if (file_exists($docRoot . "/$subRoot$commonFileName")) {
                break;
            }
        }
    }

    $self = explode("/", $subRoot);
    if (count($self) > 1) {
        $self = implode("/", array_slice($self, 0, count($self) - 1));
    } else {
        $self = "";
    }
    define("SUB_ROOT", $self);
    if (defined("ROOT_ONLY") && constant("ROOT_ONLY") === true) {
        define("PROJECT_ROOT", $_SERVER["DOCUMENT_ROOT"]);
    } else {
        define("PROJECT_ROOT", $_SERVER["DOCUMENT_ROOT"] . SUB_ROOT);
    }
}
$now = new DateTime();